# Proyecto logoPersonal


## Integrantes
Miguel Angel Veloza Ortiz


## Nombre
Logo Personal, con caracteristicas propias.

## Descripcion
El proyecto se basa en crear una imagen que represente la personalidad y gustos propios por medio de textos dibujos, imágenes, etc. La imagen se creara utilizando software libre

## Justificacion
El proyecto se realiza porque se quiere buscar una manera de identificación personal en las redes , lo cual dará un sentido de pertenencia y será el inicio para crear una marca propia.

## Objetivos
Un objetivo general
Crear una imagen que contenga características personales, que sirva como logo para las redes sociales

Objetivos específicos
	Conocer un nuevo entorno de trabajo utilizando software libre


## Licencia
cc by-sa 4.0

## Estado del proyecto
Detenido